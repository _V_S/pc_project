import sys
if __name__ == '__main__':
   path_argument = 'data.txt'
   if len(sys.argv) > 1:
       path_argument = sys.argv[1].split('=')[1]

def solver(input):
    output = strip_puct(input)
    output = strip_spaces(output)
    output = lowercase(output)
    return output

def strip_puct(input):
    return input.replace(',', '').replace('.', '')

def strip_spaces(input):
    return input.replace('  ', ' ')

def lowercase(input):
    return input.lower()

def output(input, filepath = 'output.txt'):
    f = open(filepath, 'w')
    f.write(input)
    f.close()
    
def start(filepath):
    lines = ''
    try:
        f = open(filepath, 'r')
    except:
        print('File {} cannot be opened'.format(filepath))
        return
    line = f.readline()
    while line:
        lines += solver(line)
        line = f.readline()
    f.close()
    output(lines)

start(path_argument)